<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Route::get('/createuser', function () {
//     return view('welcome');
// });

// Route::get('/modifyuser/{id}', function () {
//     return view('welcome');
// });

Route::get('/books', function () {
    return view('welcome');
});

Route::get('downloadcsv/{export_col}', 'CommonController@downloadCsv');

Route::get('downloadxml/{export_col}', 'CommonController@downloadXml');