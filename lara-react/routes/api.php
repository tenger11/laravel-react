<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//Previously Developer for Sample
// Route::resource('users', 'Api\UserController');

//List books
Route::get('books', 'Api\LibraryController@index');

//List single book
Route::get('books/{id}', 'Api\LibraryController@show');

//Create new book
Route::post('books', 'Api\LibraryController@store');

//Update book
Route::put('books', 'Api\LibraryController@store');

//Delete book
Route::delete('books/{id}', 'Api\LibraryController@destroy');