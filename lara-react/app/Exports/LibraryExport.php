<?php

namespace App\Exports;

use App\Library;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class LibraryExport implements FromCollection, WithHeadings, ShouldAutoSize
{
    protected $books;

    private $columns;

    private $headers;

    private $search_column;

    private $search_value;

    private $sort_type;

    private $sort_column;

    public function __construct(array $books)
    {
        $this->books = $books;
        $export_column = $this->books[0];
        $this->search_column = $this->books[1];
        $this->search_value = $this->books[2];
        $this->sort_type = $this->books[3];
        $this->sort_column = $this->books[4];
        switch ($export_column) {
            case 'ta':
                $this->columns = array('title', 'author');
                $this->headers = array('Title', 'Author');
                break;
            case 't':
                $this->columns = array('title');
                $this->headers = array('Title');
                break;
            case 'a':
                $this->columns = array('author');
                $this->headers = array('Author');
                break;
        }
    }
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Library::where($this->search_column, 'LIKE', "%{$this->search_value}%")->orderBy($this->sort_column, $this->sort_type)->get($this->columns);
    }

    public function headings(): array
    {
        return $this->headers;
    }
}
