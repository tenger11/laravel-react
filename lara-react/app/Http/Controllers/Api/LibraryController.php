<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Library;
use App\Http\Resources\Library as LibraryResource;
use App\http\Requests;
use Illuminate\Support\Facades\Input;

class LibraryController extends Controller
{
    /**
     * Listing a books.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $search_value = Input::get('search_value', '');
        $books = Library::where(Input::get('search_column', 'title'), 'LIKE', "%{$search_value}%")->orderBy(Input::get('sort_column', 'title'), Input::get('sort_type', 'asc'))->paginate(10);
        return LibraryResource::collection($books);
    }

    /**
     * Store .
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Create Or Update
        $book = $request->isMethod('put') ? Library::findOrFail($request->book_id) : new Library;

        $book->id = $request->input('book_id');
        $book->title = $request->input('title');
        $book->author = $request->input('author');

        if($book->save()){
            return new LibraryResource($book);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // Get book
        $book = Library::findOrFail($id);

        //return single book as resource.
        return new LibraryResource($book);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // Get book
        $book = Library::findOrFail($id);

        if($book->delete()){
            return new LibraryResource($book);
        }
    }
}
