<?php

namespace App\Http\Controllers;
use App\Library;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Response;
use App\Exports\LibraryExport;
use Spatie\ArrayToXml\ArrayToXml;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Input;

class CommonController extends Controller
{
    //Downloading Exported Data As Csv
    public function downloadCsv($export_col) {
        $export = new LibraryExport([$export_col, Input::get('search_column', 'title'), Input::get('search_value', ''), Input::get('sort_type', 'asc'), Input::get('sort_column', 'title')]);
        return Excel::download($export, 'books.csv', \Maatwebsite\Excel\Excel::CSV);
    }

    //Downloading Exported Data As Xml
    public function downloadXml($export_col) {
        switch ($export_col) {
            case 'ta':
                $columns = array('title', 'author');
                break;
            case 't':
                $columns = array('title');
                break;
            case 'a':
                $columns = array('author');
                break;
        }
        $search_value = Input::get('search_value', '');
        $collection = Library::where(Input::get('search_column', 'title'), 'LIKE', "%{$search_value}%")->orderBy(Input::get('sort_column', 'title'), Input::get('sort_type', 'asc'))->get($columns)->toArray();
        $xml = new \SimpleXMLElement('<bookstore/>');
        $this->array_to_xml($collection, $xml);
        $response = Response::make($xml->asXML(), 200);
        $response->header('Cache-Control', 'public');
        $response->header('Content-Description', 'File Transfer');
        $response->header('Content-Disposition', 'attachment; filename=books.xml');
        $response->header('Content-Transfer-Encoding', 'binary');
        $response->header('Content-Type', 'text/xml');
        return $response;
    }

    private function array_to_xml( $data, &$xml_data ) {
        foreach( $data as $key => $value ) {
            if( is_numeric($key) ){
                $key = 'book';
            }
            if( is_array($value) ) {
                $subnode = $xml_data->addChild($key);
                $this->array_to_xml($value, $subnode);
            } else {
                $xml_data->addChild("$key",htmlspecialchars("$value"));
            }
        }
    }
}
