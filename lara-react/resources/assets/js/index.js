import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter as Router, Link, Route} from 'react-router-dom';
import Books from './components/book/Books';

export default class Index extends Component {
    render() {
        return (
            <Router>
                <div>
                    <div className="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-white border-bottom box-shadow">
                        <h5 className="my-0 mr-md-auto font-weight-normal">Homework Project</h5>
                        <nav className="my-2 my-md-0 mr-md-3">
                            <Link className="p-2 text-dark" to="/">Books</Link>
                            {/* <Link className="p-2 text-dark" to="/">Users</Link> */}
                        </nav>
                    </div>
                    <div className="container body-color">
                        <Route path="/" exact component={Books} />
                        {/* Previously developed */}
                        {/* <Route path="/" exact component={Users} />
                        <Route path="/createuser" exact component={CreateUser} />
                        <Route path="/modifyuser/:id" exact component={ModifyUser} /> */}
                    </div>
                </div>
            </Router>
        );
    }
}

if (document.getElementById('app')) {
    ReactDOM.render(<Index />, document.getElementById('app'));
}
