import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import axios from 'axios';

export default class Users extends Component {
    constructor(){
        super();
        this.state = {
            data: [],
            pagination: []
        }
    }

    componentWillMount(){
        this.fetchUsers();
        // axios.get(this.state.url).then(res => {
        //     this.setState({
        //         data: res.data
        //     })
        // }).catch(err => {
        //     console.log(err);
        // })
    }

    fetchUsers(url){
        let $this = this;
        let page_url = (url) ? url : '/api/users';
        fetch(page_url)
        .then(res => res.json())
        .then(res =>{
            $this.setState({
                data: res.data
            });
            $this.makePagination(res.meta, res.links);
        }).catch(err => {
            console.log(err);
        })
    }

    makePagination(meta, links){
        let pagination = {
            current_page: meta.current_page,
            last_page: meta.last_page,
            next_page_url: links.next,
            prev_page_url: links.prev
        }

        this.setState({
            pagination: pagination
        })
    }

    onDelete(user){
        console.log(user);

        axios.delete('/api/users/'+user.id)
        .then(res => {
            console.log(res);
            const newState = this.state.data.slice();
            newState.splice(newState.indexOf(user), 1);
            this.setState({
                data: newState
            })
        }).catch(err => {
            console.log(err);
        })
    }

    onEdit(user){
        this.props.history.push(`/modifyuser/${user.id}`);
    }

    onNew(){
        this.props.history.push('/createuser');
    }

    render() {
        return (
            <div>
                <h1>
                    <span>Users</span>
                    <button onClick={this.onNew.bind(this)} className="btn btn-outline-primary pull-right">Create New User</button>
                </h1>
                <table className="table table-bordered">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.data.map((user, i) => (
                                <tr key={i}>
                                    <td>{user.id}</td>
                                    <td>{user.name}</td>
                                    <td>{user.email}</td>
                                    <td><button className="btn btn-primary" onClick={this.onEdit.bind(this, user)}>Edit</button> ||
                                        <button className="btn btn-danger" onClick={this.onDelete.bind(this, user)} >Delete</button>
                                    </td>
                                </tr>
                            )
                        )}
                    </tbody>
                </table>
                
                <nav aria-label="Page navigation example">
                    <ul className="pagination">
                        <li className={(this.state.pagination.prev_page_url) ? 'page-item' : 'page-item disabled'}>
                            <button className="page-link" onClick={this.fetchUsers.bind(this, this.state.pagination.prev_page_url)}>Previous</button>
                        </li>

                        <li className="page-item disabled"><a className="page-link text-dark" href="#">Page {this.state.pagination.current_page} of {this.state.pagination.last_page} </a></li>
                        
                        <li className={(this.state.pagination.next_page_url) ? 'page-item' : 'page-item disabled'}>
                            <button className="page-link" onClick={this.fetchUsers.bind(this, this.state.pagination.next_page_url)}>Next</button>
                        </li>
                    </ul>
                </nav>
            </div>
        )
    }
}
if(document.getElementById('users')){
    ReactDOM.render(<Users />, document.getElementById('users'));
}

