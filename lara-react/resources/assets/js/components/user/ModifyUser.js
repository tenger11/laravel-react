import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import axios from 'axios';

export default class ModifyUser extends Component {
    constructor(){
        super();
        this.state = {
            name: '',
            email: '',
            password: ''
        }

        this.handleNameChange = this.handleNameChange.bind(this);
        this.handleEmailChange = this.handleEmailChange.bind(this);
        this.handlePassChange = this.handlePassChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.onBack = this.onBack.bind(this);
    }

    handleNameChange(e){
        this.setState({
            name: e.target.value
        })
    }

    handleEmailChange(e){
        this.setState({
            email: e.target.value
        })
    }

    handlePassChange(e){
        this.setState({
            password: e.target.value
        })
    }

    handleSubmit(e){
        e.preventDefault();
        console.log(this.state);

        axios.put('/api/users/'+this.props.match.params.id, this.state)
        .then(res => {
            this.props.history.push('/');
        })
        .catch(err => {
            console.log(err);
        })
    }

    componentWillMount(){
        this.fetchUser(this.props.match.params.id);
    }

    fetchUser(id){
        axios.get(`/api/users/${id}`)
        .then(res => {
            let user = res.data.data;
            
            this.setState({
                name: user.name,
                email: user.email
            });
        }).catch(err => console.log(err));
    }

    onBack(e){
        e.preventDefault();
        this.props.history.push('/');
    }

    render() {
        return (
            <div>
                <h2>Modify User</h2>
                <form className="form-horizontal" onSubmit={this.handleSubmit}>
                    <div className="form-group">
                        <label className="control-label col-sm-2" htmlFor="name">Name:</label>
                        <div className="col-sm-12">
                            <input type="text" className="form-control" id="name" placeholder="Enter Name" name="name" value={this.state.name} onChange={this.handleNameChange} /> 
                        </div>
                    </div>
                    <div className="form-group">
                        <label className="control-label col-sm-2" htmlFor="email">Email:</label>
                        <div className="col-sm-12">
                            <input type="email" className="form-control" id="email" placeholder="Enter Email" name="email" value={this.state.email} onChange={this.handleEmailChange} /> 
                        </div>
                    </div>
                    <div className="form-group">
                        <label className="control-label col-sm-2" htmlFor="password">Password:</label>
                        <div className="col-sm-12">
                            <input type="password" className="form-control" id="password" placeholder="Enter Password" name="password" value={this.state.password} onChange={this.handlePassChange} /> 
                        </div>
                    </div>
                    <div className="form-group">
                        <div className="col-sm-offset-2 col-sm-12">
                            <input type="submit" className="btn btn-default" value="Save" /> 
                            <input type="button" className="btn btn-info float-right" onClick={this.onBack} value="Back" /> 
                        </div>
                    </div>
                </form>
            </div>
        )
    }
}
if(document.getElementById('modifyuser')){
    ReactDOM.render(<ModifyUser />, document.getElementById('modifyuser'));
}
