import React, { Component } from 'react';
import ReactDOM from 'react-dom';

export default class Pagination extends Component {
    constructor(){
        super();
    }

    render() {
        return (
            <nav aria-label="Page navigation example">
                <ul className="pagination">
                    <li className={this.props.pagination.prev_page_url ? 'page-item' : 'page-item disabled' }>
                        <button className="page-link" onClick={this.props.fetchBooks.bind(this,this.props.pagination.prev_page_url)}>
                            Previous
                        </button>
                    </li>
                    <li className="page-item disabled">
                        <a className="page-link text-dark" href="#">
                            Page {this.props.pagination.current_page} of {' '}{this.props.pagination.last_page}{' '}
                        </a>
                    </li>
                    <li className={this.props.pagination.next_page_url ? 'page-item': 'page-item disabled'}>
                        <button className="page-link" onClick={this.props.fetchBooks.bind(this,this.props.pagination.next_page_url)}>
                            Next
                        </button>
                    </li>
                </ul>
            </nav>
        );
    }
}

if (document.getElementById('pagination')) {
    ReactDOM.render(<Pagination />, document.getElementById('pagination'));
}
