import React, { Component } from 'react';
import ReactDOM from 'react-dom';

export default class Alert extends Component {
    constructor(){
        super();
        this.state = {
            hasError: false
        }
    }
    render() {
        return (
            (this.props.hasError ) ?
            <div>
                <div className={this.props.alertClass ? this.props.alertClass :"alert alert-warning alert-dismissible fade show" } role="alert">
                    {this.props.message}
                    <button type="button" className="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>

            : null
        );
    }
}

if (document.getElementById('alert')) {
    ReactDOM.render(<Alert />, document.getElementById('alert'));
}
