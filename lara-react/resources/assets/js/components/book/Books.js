import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import axios from 'axios';
import ModifyBook from './ModifyBook';
import Pagination from '../common/Pagination';
import BookModal from './BookModal';
import BookSearch from './BookSearch';

export default class Books extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            pagination: [],
            book: [],
            atuhor: '',
            search: {
                sort_column: 'title',
                sort_type: 'asc',
                search_column: 'title',
                search_value: ''
            }
        };

        this.fetchBooks = this.fetchBooks.bind(this);
        this.onAdd = this.onAdd.bind(this);
        this.onDelete = this.onDelete.bind(this);
        this.onUpdate = this.onUpdate.bind(this);
        this.onSearch = this.onSearch.bind(this);
    }

    componentWillMount() {
        this.fetchBooks();
    }

    fetchBooks(url) {
        let $this = this;
        let page_url = url ? url : '/api/books';
        axios
            .get(page_url, {params: this.state.search})
            .then(res => {
                $this.setState({
                    data: res.data.data
                });
                $this.makePagination(res.data.meta, res.data.links);
            })
            .catch(err => {
                console.log(err);
            });
    }

    makePagination(meta, links) {
        let pagination = {
            current_page: meta.current_page,
            last_page: meta.last_page,
            next_page_url: links.next,
            prev_page_url: links.prev
        };

        this.setState({
            pagination: pagination
        });
    }

    onAdd(book){
        axios
            .post('/api/books', book)
            .then(res => {
                this.fetchBooks();
            })
            .catch(err => {
                console.log(err);
            });
    }

    onDelete() {
        axios
            .delete('/api/books/' + this.state.book.id)
            .then(res => {
                const newState = this.state.data.slice();
                newState.splice(newState.indexOf(this.state.book), 1);
                this.setState({data: newState});
            })
            .catch(err => {
                console.log(err);
            });
    }

    onUpdate(author){
        let book = this.state.book;
        book.author = author;
        book.book_id = this.state.book.id;
        axios
            .put('/api/books', book)
            .then(res => {
                this.fetchBooks();
            })
            .catch(err => {
                console.log(err);
            });
    }
    //Setting Data For Update,Delete
    setData(book, author){
        this.setState({book: book, author: author});
    }

    //for Sorting
    onSort(type, col){
        const search = this.state.search;
        this.state.search.sort_type = type;
        this.state.search.sort_column = col;
        this.setState({search});
        this.fetchBooks();
    }

    //for Searching
    onSearch(col, val){
        const search = this.state.search;
        this.state.search.search_column = col;
        this.state.search.search_value = val;
        this.setState({search});
        this.fetchBooks();
    }

    render() {
        return (
            <div>
                <h1>Books</h1>
                <ModifyBook onAdd={this.onAdd} />
                <BookSearch onSearch={this.onSearch} sortType={this.state.search.sort_type} sortCol={this.state.search.sort_column} />
                <BookModal onDelete={this.onDelete} author={this.state.author} onUpdate={this.onUpdate} />
                <table className="table table-bordered">
                    <colgroup>
                        <col width="50%" />
                        <col width="45%" />
                        <col width="*" />
                    </colgroup>
                    <thead>
                        <tr>
                            <th>Title 
                                <div className="pull-right">
                                    <i className="fas fa-sort-amount-up" onClick={this.onSort.bind(this, 'asc', 'title')}></i>
                                    <i className="fas fa-sort-amount-down" onClick={this.onSort.bind(this, 'desc', 'title')}></i>
                                </div>
                            </th>
                            <th>Author 
                                <div className="pull-right">
                                    <i className="fas fa-sort-amount-up" onClick={this.onSort.bind(this, 'asc', 'author')}></i>
                                    <i className="fas fa-sort-amount-down" onClick={this.onSort.bind(this, 'desc', 'author')}></i>
                                </div>
                            </th>
                            <th>Delete</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.data.map((book, i) => (
                            <tr key={i}>
                                <td>{book.title}</td>
                                <td><a className="text-primary" data-toggle="modal" data-target="#bookModal" onClick={this.setData.bind(this, book, book.author)}>{book.author}</a></td>
                                <td className="text-center">
                                    <button className="btn btn-secondary" data-toggle="modal" data-target="#bookModal" onClick={this.setData.bind(this, book, '')} ><i className="fas fa-trash-alt"></i></button>
                                </td>
                            </tr>
                        ))}
                    </tbody>
                </table>
                <Pagination pagination={this.state.pagination} fetchBooks={this.fetchBooks} />
            </div>
        );
    }
}
if (document.getElementById('books')) {
    ReactDOM.render(<Books />, document.getElementById('books'));
}
