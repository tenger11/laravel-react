import React, { Component } from 'react';
import ReactDOM from 'react-dom';

export default class BookModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            author: ''
        };
        this.handleChange = this.handleChange.bind(this);
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.author !== this.state.author) {
          this.setState({ author: nextProps.author});
        }
    }

    handleChange(e){
        this.setState({[e.target.name] : e.target.value});
    }

    render() {
        return (
            <div>
                <div className="modal fade" id="bookModal" tabIndex="-1" role="dialog" aria-labelledby="bookModalLabel" aria-hidden="true">
                    <div className="modal-dialog" role="document">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h5 className="modal-title" id="bookModalLabel">Book</h5>
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div className="modal-body">
                                {!this.props.author ?
                                    'Are You Sure ?' :
                                    <div className="form-group">
                                        <label className="control-label col-sm-2" htmlFor="author">Author:</label>
                                        <div className="col-sm-12">
                                            <input type="text" maxLength="24" className="form-control" placeholder="Enter Author" name="author" value={this.state.author} onChange={this.handleChange} />
                                        </div>
                                    </div>
                                }
                            </div>
                            <div className="modal-footer">
                                <button type="button" className="btn btn-primary" data-dismiss="modal" onClick={!this.props.author ? this.props.onDelete : this.props.onUpdate.bind(this, this.state.author)}>Save</button>
                                <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
if (document.getElementById('bookmodal')) {
    ReactDOM.render(<BookModal />, document.getElementById('bookmodal'));
}
