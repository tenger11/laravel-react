import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import Alert from '../common/Alert';

export default class ModifyBook extends Component {
    constructor(props) {
        super(props);
        this.state = {
            book_id: '',
            title: '',
            author: '',
            alert: '',
            hasError: false,
            alertClass: 'alert alert-danger'
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(e){
        this.setState({[e.target.name] : e.target.value});
    }

    handleSubmit(e) {
        e.preventDefault();
        if (!this.state.title || !this.state.author) {
            this.setState({
                alert: 'Please fill in all required fields',
                hasError: true
            });
            setTimeout(() => this.setState({ hasError: false }), 1000);
        } else {
            this.setState({title: '',author: ''});
            this.props.onAdd(this.state);
        }
    }

    render() {
        return (
            <div>
                <Alert message={this.state.alert} hasError={this.state.hasError} alertClass={this.state.alertClass} />
                <form className="form-horizontal" onSubmit={this.handleSubmit}>
                    <div className="form-group">
                        <label className="control-label col-sm-2" htmlFor="name">
                            Title:
                        </label>
                        <div className="col-sm-12">
                            <input type="text" maxLength="50" className="form-control" placeholder="Enter Title" name="title" value={this.state.title} onChange={this.handleChange} />
                        </div>
                    </div>
                    <div className="form-group">
                        <label className="control-label col-sm-2" htmlFor="email">
                            Author:
                        </label>
                        <div className="col-sm-12">
                            <input type="text" maxLength="24" className="form-control" placeholder="Enter Author" name="author" value={this.state.author} onChange={this.handleChange} />
                        </div>
                    </div>
                    <div className="form-group">
                        <div className="col-sm-offset-2 col-sm-12">
                            <input type="submit" className="btn btn-primary" value="Save" />
                        </div>
                    </div>
                    <hr className="my-3"></hr>
                </form>
            </div>
        );
    }
}
if (document.getElementById('modifybook')) {
    ReactDOM.render(<ModifyBook />, document.getElementById('modifybook'));
}
