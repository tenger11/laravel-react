import React, { Component } from 'react';
import ReactDOM from 'react-dom';

export default class BookSearch extends Component {
    constructor(props) {
        super(props);
        this.state = {
            export_col: 'ta',
            search_col: 'title',
            search_val: '',
            search_field: 'Title',
            sort_type: '',
            sort_col: ''
        };
        this.handleChange = this.handleChange.bind(this);
    }

    //lifesycle methods
    componentWillReceiveProps(nextProps) {
        if (nextProps.sortType !== this.state.sort_type || nextProps.sortCol !== this.state.sort_col) {
          this.setState({ sort_type: nextProps.sortType, sort_col: nextProps.sortCol});
        }
    }

    //for searching
    handleChange(e){
        this.setState({[e.target.name] : e.target.value});
    }

    onSelect(col, field, e){
        e.preventDefault();
        this.setState({search_col : col, search_field: field});
    }

    onEnter(e){
        if (e.key === 'Enter') {
            this.props.onSearch(this.state.search_col, this.state.search_val);
        }
    }

    //for exporting
    onClickExp(col){
        this.setState({export_col: col});
    }

    render() {
        const export_types = [{text: 'Title & Author', value:'ta'}, {text: 'Title', value: 't'}, {text: 'Author', value: 'a'}];
        return (
            <div className="row justify-content-between">
                <div className="col col-lg-4 col-md-12 col-sm-12 btn-group mb-3" role="group" aria-label="Button group with nested dropdown">
                    <div className="btn-group" role="group">
                        <button type="button" className="btn btn-success dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i className="fas fa-download"></i>
                        </button>
                        <div className="dropdown-menu">
                            <a className="dropdown-item" href={"/downloadcsv/" + this.state.export_col + "?search_column=" + this.state.search_col + "&search_value=" + this.state.search_val + "&sort_type=" + this.state.sort_type + "&sort_column=" + this.state.sort_col } target="_blank" >CSV</a>
                            <a className="dropdown-item" href={"/downloadxml/" + this.state.export_col+ "?search_column=" + this.state.search_col + "&search_value=" + this.state.search_val + "&sort_type=" + this.state.sort_type + "&sort_column=" + this.state.sort_col } target="_blank" >XML</a>
                        </div>
                    </div>
                    {export_types.map((exp, i) => (
                         <button key={i} type="button" className={"btn btn-secondary " + (this.state.export_col === exp.value ? "btn-success" : "")} onClick={this.onClickExp.bind(this, exp.value)}>{exp.text}</button>
                    ))}
                </div>
                <div className="col col-lg-4 col-md-12 col-sm-12 input-group mb-3 w-30 pull-right">
                    <div className="input-group-prepend">
                        <button className="btn btn-outline-secondary dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{this.state.search_field}</button>
                        <div className="dropdown-menu">
                            <a className="dropdown-item" href="#" onClick={this.onSelect.bind(this, 'title', 'Title')}>Title</a>
                            <a className="dropdown-item" href="#" onClick={this.onSelect.bind(this, 'author', 'Author')}>Author</a>
                        </div>
                    </div>
                    <input type="text" onKeyDown={this.onEnter.bind(this)} placeholder="... click enter" className="form-control" aria-label="Text input with dropdown button" name="search_val" value={this.state.search_val} onChange={this.handleChange} />
                </div>
            </div>
        );
    }
}
if (document.getElementById('booksearch')) {
    ReactDOM.render(<BookSearch />, document.getElementById('booksearch'));
}
